Pod::Spec.new do |s|
s.name             = 'WPDebug'
s.version          = '0.0.1'
s.summary          = 'WPDebug'
s.description      = <<-DESC
WPDebug
DESC

s.homepage         = 'https://gitlab.com/anrikopencode/wpdebug.git'
s.author           = { 'gwp' => '819343052@qq.com' }
s.source           = { :git => 'https://gitlab.com/anrikopencode/wpdebug.git', :tag => s.version }
s.ios.deployment_target = '8.0'
s.source_files = '*.{h,m}'

s.dependency 'FLEX'
s.dependency 'Aspects'
s.dependency 'WHDebugTool'
end

