//
//  FlexTool.m
//  checkVersion
//
//  Created by Anrik on 2019/5/16.
//  Copyright © 2019 Anrik. All rights reserved.
//

#import "FlexTool.h"
#import <Aspects.h>
#import "FLEXManager.h"
#import <WHDebugTool/WHDebugToolManager.h>

@implementation FlexTool

+ (void)load {
    [self hookViewController];
}

+ (void)hookViewController {
    Class IKLoginFullScreenViewController = NSClassFromString(@"IKSearchDefaultViewController");
    if (IKLoginFullScreenViewController) {
        [IKLoginFullScreenViewController aspect_hookSelector:@selector(collectionViewRegisterCell) withOptions:AspectPositionBefore usingBlock:^(id<AspectInfo> info){
            UIViewController *instance = info.instance;
            UITapGestureRecognizer *tapges = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(fingerIncident:)];
            tapges.numberOfTouchesRequired = 3;
            tapges.numberOfTapsRequired = 2;
            [instance.view addGestureRecognizer:tapges];
        } error:nil];
    }
}

+ (void)fingerIncident:(UITapGestureRecognizer *)ges {
    [[FLEXManager sharedManager] showExplorer];
    // 这个方法调用的时候会判断监测是不是处于打开的状态，如果打开了则关闭，如果没有打开就开启。
    [WHDebugToolManager toggleWith:DebugToolTypeAll];
}

@end
